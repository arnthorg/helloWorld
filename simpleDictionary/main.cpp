#include "std_lib_facilities.h"
//input: Broccoli cauliflower and testing derp yams rather rather cauliflower

bool matchWord(vector<string>* dislike, string word) {
	for(string dislikedWord : *dislike) {
		if(word == dislikedWord) {
			return true;
		}
	}
	return false;
}
int main(int argc, char **argv)
{
	vector<string> dislike = {"Broccoli", "cauliflower", "yams"};
	vector<string> words;
	for(string temp; cin>>temp; ) {
		words.push_back(temp);
	}
	
	sort(words);
	
	for(unsigned int i=0;i<words.size();i++ ) {
		if(i==0 ||words[i-1] != words[i]) {
			if(matchWord(&dislike, words[i])) {
				cout<<"BLEEP\n";
			}
			else {
				cout<<words[i]<<"\n";
			}
		}
	}
	return 0;
}
