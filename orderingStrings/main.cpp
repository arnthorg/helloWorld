#include "std_lib_facilities.h"

int main(int argc, char **argv)
{
	string i1, i2, i3;
	cout<<"Enter three words: \n";
	cin >>i1>>i2>>i3;
	auto minimum = min(min(i1,i2), i3);
	auto maximum = max(max(i1,i2), i3);
	string median;
	if(i1 == maximum) {
		median  = max(i2, i3);
	}
	else if(i2 == maximum) {
		median  = max(i1, i3);
	}
	else if(i3 == maximum) {
		median  = max(i1, i2);
	}
	cout<< minimum<<", "<< median << ", " << maximum<< "\n";
	return 0;
}
