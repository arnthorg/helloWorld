#include "std_lib_facilities.h"

double sumVector(vector<double> *vec)
{
	double sum = 0;
	for(double x : *vec) {
		sum += x;
	}
	return sum;
}


int main(int argc, char **argv)
{
	vector<double> route;
	double distance;
	cout<<"Input distances in km: ";

	while(cin>>distance) {
		route.push_back(distance);
	}
	cout<<sumVector(&route)<<endl;
	double maxDistance = 0;
	double minDistance = 0;
	double sumDistances = 0;
	for(struct{double distance; unsigned int i;} v = {0,0}; v.i<route.size(); v.i++ ) {

		if(!v.i) {
			maxDistance = abs(route[v.i] - route[v.i+1]);
			minDistance = abs(route[v.i] - route[v.i+1]);
		} else {
			distance = abs(route[v.i]- route[v.i-1]);
			sumDistances += distance;
			if(distance > maxDistance) {
				maxDistance = distance;
			} else if ( distance < minDistance) {
				minDistance = distance;
			}
		}
	}
	cout<<"mindistance: "<<minDistance<<endl
	    <<"maxDistance: "<<maxDistance<<endl
		<<"mean distance: "<< sumDistances/(route.size()-1)<<endl;

	return 0;

}
