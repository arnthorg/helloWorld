#include "std_lib_facilities.h"

int main(int argc, char **argv)
{
	vector<string> numbers = {	"zero", "one", "two", "three", "four", "five", "six", 
								"seven", "eight", "nine"};
								
	string input;
	cout<<"Enter numbers: ";
	while(cin>>input) {
		if(input.size() == 1){
			int number = input[0]-'0';
			cout<<numbers[number]<<endl;
		}
		else {
			cout<<find(numbers.begin(), numbers.end(), input) - numbers.begin();
		}
	}
	return 0;
}
