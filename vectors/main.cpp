#include "std_lib_facilities.h"

int main(int argc, char **argv)
{
	vector<int> v = {1,2,3,6,4,8,9,4,2};
	cout<<v[7]<<endl;
	vector<int> vi(6);
	for(unsigned int i =0; i<vi.size();i++) {
	cout<<vi[i]<<endl;
	}
	cout<<"\n";
	for(int x:v) {
		cout<<x<<endl;
	}
	vector<int> vector2;
	
	vector2.push_back(2);
	vector2.push_back(9);
	vector2.push_back(5);
	vector2.pop_back();
	for(int x:vector2) {
		cout<<x<<endl;
	}
	vector<double> temperature;
	for(double temp; cin>>temp;) {
		temperature.push_back(temp);
	}
	for(double x:temperature) {
		cout<<x<<endl;
	}
	cout<<"average: ";
	double mean=0;
	for(double x:temperature) {
		mean += x;
	}
	cout<< mean/temperature.size()<<endl;
	return 0;
}
