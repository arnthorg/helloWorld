#include "std_lib_facilities.h"

int main(int argc, char **argv)
{
	auto numberOfRepeats = 0;
	string current, past = "";
	cout<<"Enter words, q to end: ";
	while(cin>>current) {
		if(current == past) {
			cout<<"Repeated word: "<< current <<endl;
			numberOfRepeats += 1;
		}
		if(current == "q") {
			break;
		}
		
		past = current;
	}
	cout<<"number of repeats: " << numberOfRepeats << "\n";
}
