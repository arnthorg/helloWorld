#include "std_lib_facilities.h"
#define meterToCM 100
#define inchToCM 2.54
#define ftToCM 12 * inchToCM

double unitMatching(string* unit)
{
	if(*unit == "m") {
		return meterToCM;
	}
	if(*unit == "cm") {
		return 1;
	}
	if(*unit == "ft") {
		return ftToCM;
	}
	if(*unit == "in") {
		return inchToCM;
	}
	return -1;
}
int main(int argc, char** argv)
{
	// vector<int> measurements;
	double i1 = 0;
	double biggest = 0;
	string biggestUnit = "";
	double smallest = -1;
	string smallestUnit = "";
	double conversion = 0;
	vector<double> measurementsInMeters;
	string unit = "";
	double sum = 0;
	int count = 0;
	while(cin >> i1 >> unit) {
		if(unitMatching(&unit) > 0) {
			conversion = i1 * unitMatching(&unit);
			measurementsInMeters.push_back(conversion/meterToCM);
			count += 1;
			sum += conversion;
			if(smallest == -1) {
				smallest = conversion;
				smallestUnit = unit;
				biggest = conversion;
				biggestUnit = unit;
			} else {
				
				if(conversion > biggest) {
					biggest = conversion;
					biggestUnit = unit;
					cout << i1 << unit << " Largest thus far\n";
				} else if(conversion < smallest) {
					smallest = conversion;
					smallestUnit = unit;
					cout << i1 << unit << " Smallest thus far\n";
				}
			}
		} else {
			cout << "Illegal unit\n";
		}
	}
	cout<<"Smallest value: "<<smallest/ unitMatching(&smallestUnit) <<smallestUnit<<endl;
	cout<<"Biggest value: "<<biggest/ unitMatching(&biggestUnit) <<biggestUnit<<endl;
	cout<<"Sum: "<<sum<<"cm"<<endl;
	cout<<"Count: "<<count<<endl;
	cout<<"Values: \n";
	sort(measurementsInMeters);
	for(double x : measurementsInMeters) {
		cout<<x<<"m"<<endl;
	}
	return 0;
}
